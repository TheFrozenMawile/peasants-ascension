  ____                            _       _
 |  _ \ ___  __ _ ___  __ _ _ __ | |_ ___( )
 | |_) / _ \/ _` / __|/ _` | '_ \| __/ __|/
 |  __/  __/ (_| \__ \ (_| | | | | |_\__ \
 |_|   \___|\__,_|___/\__,_|_| |_|\__|___/
         _                           _
        / \   ___  ___ ___ _ __  ___(_) ___  _ __
       / _ \ / __|/ __/ _ \ '_ \/ __| |/ _ \| '_ \
      / ___ \\__ \ (_|  __/ | | \__ \ | (_) | | | |
     /_/   \_\___/\___\___|_| |_|___/_|\___/|_| |_|
Peasants' Ascension -- Programmed by TheFrozenMawile using Python
 -- v1.0.0 Beta
----------------------------------------------------
Legal Stuff:

    Peasants' Ascension is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Peasants' Ascension is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Peasants' Ascension.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------
Downloads for both the standalone and source-code versions of this game
are available at <http://reddit.com/r/PeasantsAscension/>. We also have 
articles about programming, technology, and video games, so check it
out if you enjoy those things! ;)
----------------------------------------------------
How do I install this game?

    If you downloaded the standalone version (the one without the word
    "source" in its file-name) then there is no installation required!
    If you downloaded the source-code version, then running the game will
    require Python 3.6 to be installed, as well as the corresponding 
	version of Pygame. 
----------------------------------------------------
How to play:

    SETUP:
    
        1. Find the folder named "Game Files"
        2. Run the file named "PeasantsAscension.exe" in this folder
        3. Play the game
        
    PLAYING THE GAME:
    
        Peasants' Ascension is very simple. After creating a character,
        you are placed on the world map with minimal equipment. 
        To use a command, type in the command and then hit 
        Enter (Windows) or Return (Mac OS X) to send it. 
        Available commands are (almost) always listed on the 
        screen, and can often be shortened to just the first 
        letter (i.e. "x" or "e" instead of "exit"). Sometimes,
        such as on the title screen, the command is listed as 
        a word with the first letter in brackets (i.e. "[P]lay").
        In this case, just input the letter inside the brackets.
        Other times, the command is simply a number with the 
        command located next to it. (i.e. [1] View Inventory).
        Simply input the number in the brackets next to the 
        command you want to use.
----------------------------------------------------
What is the "Adventure Name"?

    When creating your character, the game will ask you to input
    an Adventure Name. The purpose of this is to give a name to the
    folder in which the save files are located. This is why there are
    so many limitations to what you can name adventures, as certain 
    symbols are not allowed as folder names on certain OSes. To prevent
    problems, the game only allows alphanumeric characters (A-Z, a-z, 0-9)
    as well as the underscore-character ( _ ) and the hyphen-character ( - ).
    In addition, the game does not allow files to be named "" (aka nothing).
    If you wish to include these characters in your Adventure Name, you will
    have to use a file/directory viewer to rename the folder to want you want,
    assuming that your OS allows those characters.
----------------------------------------------------
What to do if the game crashes:

    Due to how the Windows Command Prompt and Python work, errors
    raised through the console immediately close the game, preventing
    the error message from being read. To counteract this, the game
    automatically logs the error in a file named "error_log.out". If 
    you encounter an error, the message will be located there, and the 
    file can be sent to me (stephenbcenter@gmail.com) so I can fix the
    problem. Don't worry, the only information located in the file is
    a generic error message and nothing else. (If you want to verify 
    this, you can open the file using a program like TextWrangler, Notepad++, 
    or Sublime Text.) I appreciate your help!
----------------------------------------------------
How to change the settings:

    By default, the game saves its files in a folder called "Save Files",
    located in the Game Files directory. It also defaults to playing all
    music/sound-effects at full volume. These can both be changed by 
    editing a set of variables in a file called "settings.cfg", located in
    the "Peasants' Ascension" folder. This file can be opened using a program like
    TextWrangler, Notepad++, or Sublime Text. Instructions on changing these
    are located in the file.
----------------------------------------------------
Credits:

    Programmed by TheFrozenMawile
    Ideas & Bug-testing by Starkiller106024, Apollo Kalar, and Vexal
    Music by Eric Skiff, Ben Landis, and some others
    Sound FX by TheFrozenMawile and Apollo Kalar
    
    Full credits can be found in Credits.txt
----------------------------------------------------
Music:

    If you like the music you hear in this game, consider
    giving the composers' websites a visit:
    
        Ben Landis - http://www.benlandis.com/
        Eric Skiff - http://ericskiff.com/
    
    These composers are very talented, and I'd highly recommend
    you listen to more of their work!